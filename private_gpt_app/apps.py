from django.apps import AppConfig


class PrivateGptAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'private_gpt_app'
