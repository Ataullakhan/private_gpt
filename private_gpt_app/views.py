from django.shortcuts import render
from dotenv import load_dotenv
# from duckdb import torch
# from langchain.chains import RetrievalQA
from langchain.embeddings import HuggingFaceEmbeddings
# from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
# from langchain.vectorstores import Chroma
# from langchain.llms import GPT4All, LlamaCpp
from django.views.decorators.csrf import csrf_exempt
import os
# import argparse
# import torch
from langchain.chains import RetrievalQA
# from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.vectorstores import Chroma
from langchain.embeddings import HuggingFaceInstructEmbeddings
# from langchain.llms import HuggingFacePipeline
# from constants import CHROMA_SETTINGS, PERSIST_DIRECTORY
# from transformers import LlamaTokenizer, LlamaForCausalLM, pipeline
# import click

load_dotenv()

ROOT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))

# Define the folder for storing database
# PERSIST_DIRECTORY = f"{ROOT_DIRECTORY}/db"


embeddings_model_name = 'all-MiniLM-L6-v2'
persist_directory = f"{ROOT_DIRECTORY}/db"

model_type = 'GPT4All'
model_path = 'gpt_model/ggml-gpt4all-j-v1.3-groovy.bin'
model_n_ctx = 1000
target_source_chunks = 4

from private_gpt_app.constants import CHROMA_SETTINGS

# from django.http import JsonResponse
#
from transformers import LlamaTokenizer, LlamaForCausalLM, pipeline
from langchain.llms import HuggingFacePipeline
# from transformers import AutoTokenizer
# import transformers
# import torch


def load_model():
    """
    Select a model on huggingface.
    If you are running this for the first time, it will download a model for you.
    subsequent runs will use the model from the disk.
    """
    # model_id = "TheBloke/vicuna-7B-1.1-HF"
    model_id = "tiiuae/falcon-7b-instruct"
    tokenizer = LlamaTokenizer.from_pretrained(model_id)

    model = LlamaForCausalLM.from_pretrained(model_id,
                                    # load_in_8bit=True, # set these options if your GPU supports them!
                                    # device_map='auto',#'auto'/1,
                                    # torch_dtype=torch.bfloat16
                                               # low_cpu_mem_usage=True
                                    )

    pipe = pipeline(
        "text-generation",
        model=model,
        tokenizer=tokenizer,
        max_length=2048,
        temperature=0,
        top_p=0.95,
        repetition_penalty=1.15
    )

    # model = "tiiuae/falcon-7b-instruct"

    # tokenizer = AutoTokenizer.from_pretrained(model)
    # pipe = transformers.pipeline(
    #     "text-generation",
    #     model=model,
    #     tokenizer=tokenizer,
    #     max_length=2048,
    #     temperature=0,
    #     top_p=0.95,
    #     torch_dtype=torch.bfloat16,
    #     trust_remote_code=True,
    #     device_map="auto",
    #     repetition_penalty=1.15
    # )

    local_llm = HuggingFacePipeline(pipeline=pipe)

    return local_llm


@csrf_exempt
def main_view(request):
    print('views called')
    if request.method == 'POST':
        print('under post method')
        # Retrieve the query from the request
        query = request.POST.get('query')

        print('query=======', query)

        if query == 'exit':
            return render(request, "index.html", {})

        # Parse the command line arguments (if necessary)
        # args = parse_arguments()

        embeddings = HuggingFaceInstructEmbeddings(model_name="hkunlp/instructor-xl",
                                                   model_kwargs={"device": 'cpu'})

        # embeddings = HuggingFaceEmbeddings(model_name=embeddings_model_name)

        print('embeddings done')
        db = Chroma(persist_directory=persist_directory, embedding_function=embeddings, client_settings=CHROMA_SETTINGS)
        # db = Chroma(persist_directory=persist_directory, embedding_function=embeddings, client_settings=CHROMA_SETTINGS)

        print('db done')

        retriever = db.as_retriever()
        # retriever = db.as_retriever(search_kwargs={"k": target_source_chunks})
        print('retriever done')
        # callbacks = []  # Initialize callbacks list

        # if model_type == "LlamaCpp":
        #     llm = LlamaCpp(model_path=model_path, n_ctx=model_n_ctx, callbacks=callbacks, verbose=False)
        # elif model_type == "GPT4All":
        # llm = GPT4All(model=model_path, n_ctx=model_n_ctx, backend='gptj', callbacks=callbacks, verbose=False)
        # else:
        #     return render(request, "index.html", {})

        llm = load_model()

        print('llm model load done')

        qa = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever, return_source_documents=True)
        # qa = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever,
        #                                  return_source_documents=True)


        print('qa done')
        # Get the answer from the chain
        res = qa(query)

        print('res done', res)
        answer, docs = res['result'], res['source_documents']


        print('answer===========', answer)

        # Prepare the response data
        response_data = {
            'context': query,
            'answer': answer,
            'meta': []
        }

        # Prepare the relevant sources used for the answer
        for document in docs:
            response_data['meta'].append({
                'source': document.metadata["source"],
                'content': document.page_content
            })

        print(response_data)

        return render(request, "index.html", response_data)

    return render(request, "index.html", {})
